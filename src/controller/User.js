import _ from 'lodash'

import {
  UserModel,
  RecordModel,
  AccessTokenModel
} from '../models'

var UserController = {
  getprofile : async (req, res) => {
    if(!res.locals.userId) res.status(404).send()
    try {
      let user = await UserModel.findOne({
        where: { id: res.locals.userId },
        attributes: ['id', 'email' , 'ballance','roles']
      })
      if (!user) return res.status(404).send()
  
      let records = await RecordModel.findAll({
        where       : { UserId : res.locals.userId },
        attributes  : ['id']
      })
      user.dataValues.count = records.length
  
      return res.status(200).send(user)
    } catch (e) {
      console.log("===========================")
      console.log(e)
      return res.status(500).json(e)
    }
  } ,
  signOut : async (req, res) => {
    if(!res.locals.userId) return res.status(400).send()
    try {
        let user = await UserModel.findOne({
            where: {
              id: res.locals.userId,
            },
            attributes: ['id', 'accessToken']
          })
        
        if (!user) return res.status(404).send()
        AccessTokenModel.delToken(user.accessToken)
        user.accessToken = null
        user.save().then( (newuser) => {return res.status(201).send()}
        ).catch( (e) => res.status(500).send(e))

    } catch(e) {
      return res.status(500).json(e)
    }
    },
  addBallance : async (req, res) => {
    try {
      if (!_.has(req.body, 'amount')) return res.status(400).send()
      if(!res.locals.userId) res.status(404).send()
      let user = await UserModel.findById(res.locals.userId)
      if (!user) return res.status(404).send()
      user.ballance = parseFloat(user.ballance) + parseFloat(req.body.amount)
      user = await user.save({ fields: ['ballance'] })
      return res.status(204).send()
    } catch (e) {
      return res.status(500).json(e)
    }
  },
}


export default UserController
