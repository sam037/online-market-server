import AuthController from './Auth'
import ProductController from './Product'
import SearchController from './Search'
import UserController from './User'

export {
    AuthController,
    ProductController,
    SearchController,
    UserController,
}
