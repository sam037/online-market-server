import _ from 'lodash'

import {
  ProductModel,
} from '../models'


var SearchController = {
  advance :async (req, res) => {
    // just for Version 1 should be optimized for future version
    try {
      let products = await ProductModel.findAll()
      let results = []
      for(let i=0; i<products.length; i++) {
        if(req.query.category.split(',').length > 1 ){
          if(products[i].title.toLowerCase().indexOf(req.query.keyword.toLowerCase())!=-1 && _.intersection(products[i].category, req.query.category.split(',')).length > 0) {
            results.push(products[i])
          }
        } else {
          if(products[i].title.toLowerCase().indexOf(req.query.keyword.toLowerCase())!=-1){
            results.push(products[i])
          }
        }
      }
      return res.status(200).send(results)
    } catch (e) {
      return res.status(500).json(e)
    }
  } ,
  basic : async (req, res) => {
    // just for Version 1 should be optimized for future version
    try {
      let products = await ProductModel.findAll({attributes:['id', 'title','price']})
      let results = []
      for(let i=0; i<products.length; i++) {
        if(products[i].title.toLowerCase().indexOf(req.query.keyword.toLowerCase())!=-1) {
          results.push(products[i])
        }
      }
      return res.status(200).send(results)
    } catch (e) {
      return res.status(500).json(e)
    }
  }

}

export default SearchController
