import _ from 'lodash'

import {
  Router
} from 'express'

import {
  ProductModel,
  RecordModel,
  UserModel,
  AccessTokenModel,
  CartModel,
  UserCartAcc,
  ProductAcc,
  ProductAcc2,
  UserAcc,
  UserAcc2,
  UserCartAcc2
} from '../models'

import {
  Constants,
} from '../utilities'

import path from 'path'


const router = Router()


var ProductController = { 
  add : async (req, res) => {
    try {
      /*
      if(!res.locals.userId) return res.status(403).send()
      let user = await UserModel.findOne({
        where: { id: res.locals.userId },
      })
      
      if(! _.includes(user.roles,'admin')) return res.status(403).send()
      */
      if (!_.has(req.body,'title')) return res.status(400).send()
      console.log(res.req.files);
      
      
      const newProduct = {
        fileName    : res.req.files.fileToSave[0].filename ,
        thumbnail   : res.req.files.thumbnail[0].filename ? res.req.files.thumbnail[0].filename  :  'default.jpeg',
        description : req.body.description,
        title       : req.body.title,
        price       : req.body.price,
        category    :   _.intersection(Constants.CATEGORY,JSON.parse(req.body.category)),
      }
      let product = await ProductModel.create(newProduct)
      return res.status(200).json(product)
    } catch (e) {
      console.log("===============================================");
      console.log(e);
      return res.status(500).json(e)
    }
  },
  submitCart : async (req, res) => {
    try {
      if (!_.has(req.params, 'cartid')) return res.status(400).send()
      let cart = await CartModel.findById(req.params.cartid)
      console.log("==============================");
      console.log("SUBMIT CART");
      let length = cart.Product.length
      for(let prodId of cart.Product ){
        console.log("CART ID : ",cart.userId);
        console.log("PROD ID : ",prodId);
        let record = await RecordModel.findById(prodId)
        record.State = 1
        await record.save()
      }
      cart.Recipt = "Completed"
      await cart.save()
      return res.status(200).send(cart)
    } catch (e) {
      console.log(e);
      return res.status(500).json(e)
    }
  },
  payCart : async (req, res) => {
    try {
      let records = await RecordModel.findAll({
      where : {userId : res.locals.userId , State : 0},
      include: [ProductAcc2]})
      //if (!user) return res.status(404).send()
      let TotPrice = 0
      let Ids = []
      if(records){
        records.forEach((prod)=>{
          TotPrice += parseFloat(prod.productrecord.price)
          Ids.push(prod.id)
        })
      }    
      console.log(Ids)
      let cartRecord = await CartModel.create({
        Product : Ids,
        Price : TotPrice,
        userId : res.locals.userId,
      },{
        include : [UserCartAcc]
      })
      /*
      send request to Gate
      redirect to generated link
      */
     console.log("==============================");
     console.log(cartRecord.id,`http://localhost:4848/product/submitcart/${cartRecord.id}`)
     console.log("==============================");
     return res.redirect(`http://localhost:4848/product/submitcart/${cartRecord.id}`);
    //return res.status(200).send(cartRecord)
    } catch (e) {
      console.log(e);
      return res.status(500).json(e)
    }
  },
  index: async (req, res) => {
    try {
      const category = req.query.category ? req.query.category.split(',').map(String).filter(Boolean) : []
      let products = await ProductModel.findAll({
        //limit: 10 ,
       })
      const filteredProducts = products.filter(
        product => {
        let inCategory = _.isEmpty(category) ? true : !_.isEmpty(_.intersection(product.category, category))
        return inCategory
      })
  
      return res.status(200).json(filteredProducts)
    } catch (e) {
      console.log("======================================================");
      console.log(e);
      return res.status(500).json(e)
    }
  },
  detail : async (req, res) => {
    try {
      if (!_.has(req.params, 'productId')) return res.status(400).send()
      let product = await ProductModel.findOne({
        where: {id: req.params.productId},
        })
      if (!product) return res.status(404).send()
      if (res.locals.userId) {
        let permission = await RecordModel.findOne({
                where: {
                  UserId    : res.locals.userId,
                  ProductId : req.params.productId,
                },
              })
        if (!permission){
          product.dataValues.status = '1'
          return res.status(200).json(product)
  
        }
        product.dataValues.status = '2'
        return res.status(200).json(product)
      } else {
        product.dataValues.status = '0'
        return res.status(200).json(product)
      }
  
    } catch (e) {
      return res.status(500).json(e)
    }
  },
  addtoCart: async (req, res) => {
    try {
      if (!_.has(req.body, 'productId')) return res.status(400).send()
      let product = await ProductModel.findOne({
        where: {id: req.body.productId},
        })
      if (!product) return res.status(404).send()
      let user = await UserModel.findById(res.locals.userId)
      if (!user) return res.status(404).send()
      
      let newRecord = await RecordModel.create({
        State     : 0,
        userId    : parseInt(res.locals.userId),
        productId : parseInt(req.body.productId),
        },{
          include: [UserAcc,ProductAcc]
        })
      if (!newRecord) return res.status(500).send()
      let record = await newRecord.save()
      return res.status(201).send(record)
    } catch (e) {
      console.log(e);
      return res.status(500).json(e)
    }
  },
  removeFromCart: async (req, res) => {
    try {
      if (!_.has(req.body, 'recordId')) return res.status(400).send()
      let record = await RecordModel.findById(req.body.recordId)
      if(parseInt(record.userId) != parseInt(res.locals.userId) ) return res.status(400).send()
      RecordModel.destroy({ where: { id : record.id }}).then((deletedRecord) => {
        if(deletedRecord === 1){
            res.status(200).json({message:"Deleted successfully"});          
        }
        else { res.status(404).json({message:"record not found"}) }
        }).catch(function (error){
          res.status(500).json(error);
      });
    } catch (e) {
      console.log(e);
      return res.status(500).json(e)
    }
  },
  buyTotal : async (req, res) => {
    try {
      console.log("buy");
      
      let records = await RecordModel.findAll({
        where : {userId : res.locals.userId , State : 1},
        include: [ProductAcc2]})
      //if (!user) return res.status(404).send()
      let TotPrice = 0
      console.log(records);
      if(records){
        records.forEach((prod)=>{
          TotPrice += prod.productrecord.price
        })
      }
      
      //cart.forEach((record) => cart.totalPrice += record.Products[0].price)
      //cart.Products.forEach((prod) => cart.totalPrice += prod.price)
      //return res.status(200).send({records,TotPrice})
      return res.status(200).send(records)
    } catch (e) {
      console.log(e);
      return res.status(500).json(e)
    }
  },
  cartTotal : async (req, res) => {
    try {
      /*
      let user = await UserModel.findAll({
        where : {id : res.locals.userId},
        include: [ProductAcc]
      })
      */
      
      let records = await RecordModel.findAll({
        where : {userId : res.locals.userId , State : 0},
        include: [ProductAcc2]})
      //if (!user) return res.status(404).send()
      let TotPrice = 0
      if(records){
        records.forEach((prod)=>{
          TotPrice += prod.productrecord.price
        })
      }
      
      //cart.forEach((record) => cart.totalPrice += record.Products[0].price)
      //cart.Products.forEach((prod) => cart.totalPrice += prod.price)
      //return res.status(200).send({records,TotPrice})
      return res.status(200).send(records)
    } catch (e) {
      console.log(e);
      return res.status(500).json(e)
    }
  },
  top : async (req, res) => {
    // just for Version 1 should be optimized for future version
    try {
      let Products = await ProductModel.findAll(
        {
        include: [ProductAcc]
        }
      )
      Products.forEach( (product) => {
        product.dataValues.records = product.dataValues.records.length
      })
      Products = _.orderBy(Products, ['records'], ['desc']);
      Products = Products.slice(0, 4)
      return res.status(200).send(Products)
    } catch (e) {
      console.log("========================= ERROR ======================")
      console.log(e)
      console.log("=====================================================")
      return res.status(500).json(e)
    }
  },
  download : async (req, res) => {
    try {
      if (!_.has(req.params, 'productId')) return res.status(400).send()
      let uid = await AccessTokenModel.getTokenAsync(req.query.param)
      console.log("=================================");
      let product = await ProductModel.findOne({
        where: {id: req.params.productId},
        })
      if (!product) return res.status(404).send()
  
      if (uid) {
        console.log(uid);
        let permission = await RecordModel.findOne({
                where: {
                  UserId    : uid,
                  ProductId : req.params.productId,
                },
              })
  
        if (!permission){
          return res.status(403).send()
        }
        //let buffer = fse.readFileSync(`./files/pdf/${product.fileName}`);
        //let bufferBase64 = new Buffer(buffer);
        //return  res.status(200).send(bufferBase64);
        return res.status(200).sendFile(product.fileName, { root: path.join(__dirname, '../../files/pdf') })
      }
      return res.status(403).send()
    } catch (e) {
      console.log("======================================================");
      console.log(e);
      return res.status(500).json(e)
    }
  }
}


export default ProductController
