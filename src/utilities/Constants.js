export const MYSQL = {
  USERNAME: 'root',
  PASSWORD: '7',
  HOST:     'localhost',
  DATABASE: 'STORE'
}

export const AUTH_TOKEN_EXPIRES = 2419200

export const CATEGORY = [
  'ENG',
  'SWE',
  'HWE',
  'IT',
  'AI'
]


export const SECRET_KEY = {
  REGISTER: 'RegisterSecretKey',
  PASSWORD: 'PasswordSecretKey',
  EMAIL:    'EmailSecretKey',
  PHONE:    'PhoneSecretKey',
}
