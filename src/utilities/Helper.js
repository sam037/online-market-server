import cryptoJS from 'crypto-js'
import {Constants} from '../utilities'
import request       from 'request'
import { AccessTokenModel } from '../models'
/*
====== Generating phone code =====
*/
export const randomTokenGenerator = () => {
  let r = 'xxxxxxxxxxxxxxxxxxxx'.replace(/[x]/g, (c) => {
    let v = Math.random() * 16 | 0
    return v.toString(16)
  }).toLowerCase()
  let d = Date.now().toString().substring(0, 12)
  return `${r.slice(0, 10)}${d}${r.slice(10)}`
}
/*
====== Generating phone code =====
*/
export const randomNameGenerator = () => {
  let r = 'xxxxxxxxxx'.replace(/[x]/g, (c) => {
    let v = Math.random() * 16 | 0
    return v.toString(16)
  }).toLowerCase()
  return `${r}`
}
/*
====== Generating phone code =====
*/
export const getDateAfterDuration = (date, minutes) => {
  return new Date(date.getTime() + (minutes*60000))
}
/*
====== Generating phone code =====
*/
export const encrypt = (data, key) => {
  return encodeURIComponent(cryptoJS.AES.encrypt(JSON.stringify(data), key).toString())
}

export const decrypt = (encrypted, key, res) => {
  let bytes = cryptoJS.AES.decrypt(decodeURIComponent(encrypted), key)
  try {
    let decrypted = JSON.parse(bytes.toString(cryptoJS.enc.Utf8))
    return decrypted
  } catch (e) {
    return res.status(500).send()
  }
}
/*
====== Generating phone code =====
*/
export const generateVerifyCode = () => {
  return 'xxxxx'.replace(/[x]/g, (c) => {
    let r = Math.random() * 10 | 0
    return r.toString(10)
  })
}
/*
====== Generating phone code =====
*/
export const generateNewAccessToken = (user) => {
  const accessToken = randomTokenGenerator()
  user.accessToken = accessToken
  return user.save({ fields: ['accessToken'] })
    .then(() => {
      AccessTokenModel.setToken(accessToken, user.id ,user.roles)
      return { token: accessToken, id: user.id }
    })
    .catch(err => {
      throw err
    })
}

/*
====== Generating phone code =====
*/
export const generatePhoneCode = () => {
  return 'xxxxx'.replace(/[x]/g, (c) => {
    let r = Math.random() * 10 | 0
    return r.toString(10)
  })
}

