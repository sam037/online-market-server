import express    from 'express'
import bodyParser from 'body-parser'
import cors       from 'cors'
import routes     from './routes'

const corsConfig = {
  'origin': '*',
  'credentials': true,
  'preflightContinue': true,
  'methods': 'GET,POST,PUT,DELETE,OPTIONS',
  'allowedHeaders': 'Content-Type,x-access-key,x-user-info,roles',
  'exposedHeaders': 'x-access-key,x-user-info,roles'
}

const app = express()

/* app config */
app.use(express.json())
app.options('*', cors(corsConfig))
app.use('*', cors(corsConfig))
app.use('/files', express.static('./files/pdf'));
app.use('/thumb', express.static('./files/thumbnail'));

/* app routes */
app.use(routes)

app.listen(4848, () => {
  console.log('\x1b[32m%s\x1b[0m', 'Auction Market is Running at: http://localhost:4848')
})
