import {
    Router
  } from 'express'

import {
    Images,
  } from '../middlewares'
  
import {AuthController} from '../controller'

const router = Router()

router.post('/register/signin',AuthController.signin)

router.post('/register/signup' ,AuthController.signup)

router.post('/register/verify', AuthController.verify)

router.post('/register/resend' ,AuthController.resend)

router.post('/password/reset' ,AuthController.resetPass)

router.put('/password/submit', AuthController.submitPass)

router.put('/register/change', AuthController.change)

router.get('/register/checkuser',  AuthController.checkUser)

router.get('/register/checkphone', AuthController.checkPhone)

router.get('/register/checkemail', AuthController.checkEmail)

router.get('/password/verify', AuthController.verifyPass)



export default router
