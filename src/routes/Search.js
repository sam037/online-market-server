import _ from 'lodash'
import {
  Router
} from 'express'
import {SearchController } from '../controller'


const router = Router()
/*
Search route
*/
router.get('/advance', SearchController.advance)

router.get('/basic', SearchController.basic)

export default router
