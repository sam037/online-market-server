import _ from 'lodash'
import {
  Router
} from 'express'

import {
  AuthMW
} from '../middlewares'
import { UserController } from '../controller'
const router = Router()
/*
  USER SIGN IN
  works fine
*/

router.get('/getprofile', AuthMW.checkTokenValidation, UserController.getprofile)

router.post('/signout',AuthMW.checkTokenValidation,UserController.signOut)

export default router
