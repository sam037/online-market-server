import {
  Router
} from 'express'
import Auth 	from './Auth'
import User 	from './User'
import Product 	from './Product'
import Search from './Search'
const router = Router()

router.use('/authentication', Auth)
router.use('/user', User)
router.use('/product', Product)
router.use('/search', Search)

export default router
