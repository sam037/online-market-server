import _ from 'lodash'
import {
  Router
} from 'express'
import { ProductController} from '../controller'
import {
  Helper
} from '../utilities'
import {
  AuthMW
} from '../middlewares'
import path from 'path'
import fse from 'fs-extra'
import filepreview from 'filepreview';
import multer from 'multer'
import Sequelize from 'sequelize'


const router = Router()
/*
  Add products with file
*/
var storage = multer.diskStorage({
  destination: function (req, file, cb) {
    let fileName = file.originalname.split('.')
    if(fileName[fileName.length - 1] == 'jpg' || fileName[fileName.length - 1] == 'jpeg' ){
      cb(null, 'files/thumbnail')
    } else {
      cb(null, 'files/pdf')
    }
  },
  filename: function (req, file, cb) {
    cb(null,  `${Helper.randomNameGenerator()}${Date.now()}${path.extname(file.originalname)}`)
  }
})
const upload = multer({
    storage,
    limits: {fileSize: 10000000, files: 2},
    fileFilter:  (req, file, cb) => {
      if (!_.includes(['.zip', '.rar', '.pdf','.jpg','.jpeg'], path.extname(file.originalname))) {
        let error = new Error('Only compress or pdf are allowed!')
        error.code = 'LIMIT_UNEXPECTED_EXT'
        return cb(error, false)
      }
      cb(null, true)
    }
}).fields([
  {name : 'fileToSave' , maxCount : 1},
  {name : 'thumbnail' , maxCount : 1},])


router.post('/add', upload , ProductController.add )
/* GET PRODUCTS */
router.get('/', ProductController.index )
/* GET One PRODUCT WITH ITS DETAIL */
router.get('/detail/:productId', AuthMW.checkUserSignedIn, ProductController.detail)
/* GET Buy Products */
router.post('/addtocart' , AuthMW.checkUserSignedIn, ProductController.addtoCart)
router.post('/removefromcart' , AuthMW.checkUserSignedIn, ProductController.removeFromCart)
/* Get cart */
router.get('/carttotal' , AuthMW.checkUserSignedIn, ProductController.cartTotal)
router.get('/submitcart/:cartid' , ProductController.submitCart)
router.get('/paycart' , AuthMW.checkUserSignedIn, ProductController.payCart)
router.get('/buytotal' , AuthMW.checkUserSignedIn, ProductController.buyTotal)
/* GET top Products */
router.get('/top', ProductController.top)
/* Send Files */
router.get('/download/:productId', ProductController.download)

export default router
