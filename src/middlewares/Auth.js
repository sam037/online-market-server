import {
  AccessTokenModel
} from '../models'

export const checkTokenValidation = async (req, res, next) => {
  if (!req.get('x-access-key')) return res.status(400).end()

  try {
    let uid = await AccessTokenModel.getTokenAsync(req.get('x-access-key'))
    if (!uid) return res.status(401).end()
    res.locals.userId = uid
    AccessTokenModel.setToken(req.get('x-access-key'), uid)
    next()
  } catch (e) {
    return res.status(500).end()
  }
}

export const checkUserSignedInBeforUpload = async (req, res, next) => {
  try {
    if (req.get('x-access-key')) {
      let uid = await AccessTokenModel.getTokenAsync(req.get('x-access-key'))
      if (uid) {
        res.locals.userId = uid
        AccessTokenModel.setToken(req.get('x-access-key'), uid)
        next()
      }else {
        return res.status(403).end()
      }

    } else {
      return res.status(403).end()
    }
  } catch (e) {
    return res.status(500).end()
  }
}

export const checkUserSignedIn = async (req, res, next) => {
  try {
    if (req.get('x-access-key')) {
      let uid = await AccessTokenModel.getTokenAsync(req.get('x-access-key'))
      if (uid) {
        res.locals.userId = uid
        AccessTokenModel.setToken(req.get('x-access-key'), uid)
      }
    }
    next()
  } catch (e) {
    return res.status(500).end()
  }
}
