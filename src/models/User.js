import _ from 'lodash'
import {
  STRING,
  DECIMAL,
  BOOLEAN,
  JSON,
  INTEGER
} from 'sequelize'
import {
  Configs,
  Constants
} from '../utilities'

const UserModel = Configs.Mysql.define('user', {
  id: {
      type: INTEGER,
      primaryKey: true,
      allowNull: false,
      autoIncrement: true,
      unique: true
    },
  password: {
      type: STRING(50),
      allowNull   : false
    },
  accessToken: {
      type: STRING(32),
      unique      : true
    },
  roles: {
    type: JSON,
    allowNull: false,
    defaultValue: ['user']
    },
  phone         : { type: STRING(11),
    unique      : true,
    validate    : { isNumeric: true } },
  username :  {
    type: STRING(50),
    unique: true,
    },
}, {
})
//UPDATE users SET roles = JSON_ARRAY("admin","user");


export default UserModel
