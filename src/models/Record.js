import {
  INTEGER,
} from 'sequelize'
import {
  Configs
} from '../utilities'

const RecordModel = Configs.Mysql.define('record', {
  id: {
    type: INTEGER,
    primaryKey: true,
    allowNull: false,
    autoIncrement: true,
    unique: true
    },
  State : {
    type: INTEGER,
    allowNull: false,
    defaultValue: 0,
  },
})


export default RecordModel
/*
  UserId: {
    type: INTEGER,
    references: 'user',
    referencesKey: 'id',
    allowNull: false
    },
  ProductId: {
    type: INTEGER,
    references: 'product',
    referencesKey: 'id',
    allowNull: false
  },
*/