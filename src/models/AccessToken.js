import {
  promisify
} from 'util'
import {
  Configs,
  Constants
} from '../utilities'

export const setToken = (token, uid) => {
  Configs.Redis.set(token, String(uid), 'EX', Constants.AUTH_TOKEN_EXPIRES)
}

export const getTokenAsync = promisify(Configs.Redis.get).bind(Configs.Redis)

export const delToken = (token) => {
  Configs.Redis.del(token)
}