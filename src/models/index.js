import {
  Configs
} from '../utilities'
import * as AccessTokenModel from './AccessToken'
import UserModel             from './User'
import ProductModel          from './Product'
import RecordModel           from './Record'
import CartModel             from './Cart'
/* Association */
let UserAcc = UserModel.hasMany(RecordModel);
let UserCartAcc = UserModel.hasMany(CartModel);

let ProductAcc = ProductModel.hasMany(RecordModel);

let UserCartAcc2 = CartModel.belongsTo(UserModel, {
  as: 'usercart',
  foreignKey: 'userId',
});
let UserAcc2 = RecordModel.belongsTo(UserModel, {
  as: 'userrecord',
  foreignKey: 'userId',
});

let ProductAcc2 = RecordModel.belongsTo(ProductModel,{
  as: 'productrecord',
  foreignKey: 'productId',
});


/* Sync */
//Configs.Mysql.sync({ force: true })
Configs.Mysql.sync()

export {
  AccessTokenModel,
  UserModel,
  ProductModel,
  RecordModel,
  CartModel,
  UserAcc,
  UserCartAcc,
  ProductAcc,
  UserAcc2,
  ProductAcc2,
  UserCartAcc2,
}
/*

let UserAcc =  UserModel.belongsToMany(ProductModel,
  {
    as: 'Products',
    through: { model   : RecordModel,
              unique  : false },
    foreignKey: 'UserId' }
  );
let ProductAcc = ProductModel.belongsToMany(UserModel,
  {
    as: 'Users',
    through: { model   : RecordModel,
              unique  : false },
    foreignKey: 'ProductId' }
  );


let UserAcc = UserModel.hasMany(RecordModel);
let ProductAcc = ProductModel.hasMany(RecordModel);
let UserAcc2 = RecordModel.belongsTo(UserModel, {
  as: 'userrecord',
  foreignKey: 'userId',
});
let ProductAcc2 = RecordModel.belongsTo(ProductModel,{
  as: 'productrecord',
  foreignKey: 'productId',
});
*/