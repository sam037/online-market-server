import {
  STRING,
  TEXT,
  JSON,
  DECIMAL,
  INTEGER
} from 'sequelize'
import {
  Configs
} from '../utilities'

const ProductModel = Configs.Mysql.define('product', {
  id: {
      type: INTEGER,
      primaryKey: true,
      allowNull: false,
      autoIncrement: true,
      unique: true
    },
  fileName: {
    type: STRING(50),
    allowNull: false,
    unique: true
    },
  thumbnail: {
    type: STRING(50),
    unique: true
    },
  title: {
    type: STRING(200),
    allowNull: false
    },
  description: {
    type: TEXT('long')
    },
  counter: {
    type: DECIMAL(10, 1),
    defaultValue: 0 ,
    allowNull: false,
    validate: { min: 0 }
    },
  category: {
    type: JSON,
    defaultValue: []
    },
  price:  {
    type: DECIMAL(10, 1),
    allowNull: false,
    validate: { min: 0 }
    },
})


export default ProductModel
