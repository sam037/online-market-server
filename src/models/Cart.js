import {
    INTEGER ,
    STRING,
    JSON,
} from 'sequelize'
import { Configs } from '../utilities'
  
const Cart = Configs.Mysql.define('cart', {
    id: {
        type: INTEGER,
        primaryKey: true,
        allowNull: false,
        autoIncrement: true,
        unique: true
        },
    Product: {
        type: JSON,
        },
    Price : {
        type : INTEGER,
        defaultValue : 0 ,
    },
    Recipt : {
        type : STRING,
        defaultValue : 'Created'
    }

})
  
export default Cart
